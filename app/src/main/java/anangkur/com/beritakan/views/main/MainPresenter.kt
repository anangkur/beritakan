package anangkur.com.beritakan.views.main

import anangkur.com.beritakan.models.Article
import anangkur.com.beritakan.models.ResponseApiHome
import anangkur.com.beritakan.views.api.NewsApiService
import anangkur.com.beritakan.views.api.ServiceGenerator
import anangkur.com.beritakan.views.base.Presenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter:Presenter<MainView>{

    private var mView: MainView? = null

    override fun onAttach(view:MainView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun showFragment() {
        val newsApiService = ServiceGenerator.createService(NewsApiService::class.java)

        println("data breaking news: "+ newsApiService?.let { getBreakingNews(it).toString() })
        println("data sports news: "+ newsApiService?.let { getSportsNews(it).toString() })
        println("data business news: "+ newsApiService?.let { getBusinessNews(it).toString() })
        println("data tech news: "+ newsApiService?.let { getTechNews(it).toString() })
        println("data entertainment news: "+ newsApiService?.let { getEntertainmentNews(it) })

        newsApiService?.let { getBreakingNews(it) }
        newsApiService?.let { getSportsNews(it) }
        newsApiService?.let { getBusinessNews(it) }
        newsApiService?.let { getTechNews(it) }
        newsApiService?.let { getEntertainmentNews(it) }
    }

    private fun getBreakingNews(newsApiService: NewsApiService) {
        var breakingData: ResponseApiHome?
        newsApiService.getHeadlines("id", "261d82dd7e26494e841fb1039a4fdaf7").enqueue(object : Callback<ResponseApiHome>{
            override fun onResponse(call: Call<ResponseApiHome>, response: Response<ResponseApiHome>) {
                if (response.isSuccessful){
                    breakingData = response.body()
                    if (breakingData?.status.equals("ok")){
                        println("status: "+breakingData?.status)
                        println("totalResult: "+ breakingData?.totalResults.toString())
                        mView?.onBreakingNewsReady(breakingData)
                    }
                }
            }
            override fun onFailure(call: Call<ResponseApiHome>, t: Throwable) {
                println("error get headlines!")
            }
        })
    }

    private fun getSportsNews(newsApiService: NewsApiService) {
        var sportsData: ResponseApiHome?
        newsApiService.getNewsByCategories("id", "261d82dd7e26494e841fb1039a4fdaf7", "sports").enqueue(object : Callback<ResponseApiHome>{
            override fun onFailure(call: Call<ResponseApiHome>, t: Throwable) {
                println("error get sports news!")
            }

            override fun onResponse(call: Call<ResponseApiHome>, response: Response<ResponseApiHome>) {
                if (response.isSuccessful){
                    sportsData = response.body()
                    if (sportsData?.status.equals("ok")){
                        println("status: "+sportsData?.status)
                        println("totalResult: "+ sportsData?.totalResults.toString())
                        mView?.onSportsNewsReady(sportsData)
                    }
                }
            }
        })
    }

    private fun getBusinessNews(newsApiService: NewsApiService) {
        var businessData: ResponseApiHome?
        newsApiService.getNewsByCategories("id", "261d82dd7e26494e841fb1039a4fdaf7", "business").enqueue(object : Callback<ResponseApiHome>{
            override fun onFailure(call: Call<ResponseApiHome>, t: Throwable) {
                println("error get business news!")
            }

            override fun onResponse(call: Call<ResponseApiHome>, response: Response<ResponseApiHome>) {
                if (response.isSuccessful){
                    businessData = response.body()
                    if (businessData?.status.equals("ok")){
                        println("status: "+businessData?.status)
                        println("totalResult: "+ businessData?.totalResults.toString())
                        mView?.onBusinessNewsReady(businessData)
                    }
                }
            }
        })
    }

    private fun getTechNews(newsApiService: NewsApiService){
        var techData: ResponseApiHome?
        newsApiService.getNewsByCategories("id", "261d82dd7e26494e841fb1039a4fdaf7", "technology").enqueue(object : Callback<ResponseApiHome>{
            override fun onFailure(call: Call<ResponseApiHome>, t: Throwable) {
                println("error get technology news!")
            }

            override fun onResponse(call: Call<ResponseApiHome>, response: Response<ResponseApiHome>) {
                if (response.isSuccessful){
                    techData = response.body()
                    if (techData?.status.equals("ok")){
                        println("status: "+techData?.status)
                        println("totalResult: "+ techData?.totalResults.toString())
                        mView?.onTechNewsReady(techData)
                    }
                }
            }
        })
    }

    private fun getEntertainmentNews(newsApiService: NewsApiService){
        var entertainmentData: ResponseApiHome?
        newsApiService.getNewsByCategories("id", "261d82dd7e26494e841fb1039a4fdaf7", "entertainment").enqueue(object : Callback<ResponseApiHome>{
            override fun onFailure(call: Call<ResponseApiHome>, t: Throwable) {
                println("error get technology news!")
            }

            override fun onResponse(call: Call<ResponseApiHome>, response: Response<ResponseApiHome>) {
                if (response.isSuccessful){
                    entertainmentData = response.body()
                    if (entertainmentData?.status.equals("ok")){
                        println("status: "+entertainmentData?.status)
                        println("totalResult: "+ entertainmentData?.totalResults.toString())
                        mView?.onEntertainmentNewsReady(entertainmentData)
                    }
                }
            }
        })
    }

    open fun onClickBtnSelengkapnya(berita: Article){
        mView?.goToisiBeritaActivity(berita)
    }

    open fun onClickSubheadline(berita: Article){
        mView?.goToisiBeritaActivity(berita)
    }
}