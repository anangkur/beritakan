package anangkur.com.beritakan.views.main.adapters

import anangkur.com.beritakan.R
import anangkur.com.beritakan.models.Article
import anangkur.com.beritakan.views.isiberita.IsiBeritaActivity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class SubheadlineNewsAdapter(private val postData: List<Article>) : RecyclerView.Adapter<SubheadlineNewsAdapter.HolderData>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderData {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_berita, parent, false)
        val holderData = HolderData(v)

        return holderData
    }

    override fun onBindViewHolder(holder: HolderData, position: Int) {

        holder.berita = postData.get(position)

        holder.changeFonts()

        holder.txt_title_item_berita.text = postData.get(position).title
        Picasso.with(holder.context)
            .load(postData.get(position).urlToImage)
            .fit()
            .centerCrop()
            .into(holder.img_item_berita)
    }

    override fun getItemCount(): Int {
        return postData.size
    }

    inner class HolderData(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var img_item_berita: ImageView
        var txt_title_item_berita: TextView

        var berita: Article? = null

        var context: Context

        init {
            context = itemView.context
            itemView.setOnClickListener(this)
            img_item_berita = itemView.findViewById(R.id.img_item_berita) as ImageView
            txt_title_item_berita = itemView.findViewById(R.id.txt_title_item_berita) as TextView
        }

        fun changeFonts(){
            val chapaza = Typeface.createFromAsset(context.getAssets(), "Chapaza.ttf")

            txt_title_item_berita.setTypeface(chapaza)
        }

        override fun onClick(view: View) {
            val intent = Intent(context, IsiBeritaActivity::class.java)
            intent.putExtra("author", berita?.author)
            intent.putExtra("content", berita?.content)
            intent.putExtra("description", berita?.description)
            intent.putExtra("publishedAt", berita?.publishedAt)
            intent.putExtra("source", berita?.source?.name)
            intent.putExtra("title", berita?.title)
            intent.putExtra("url", berita?.url)
            intent.putExtra("urlToImage", berita?.urlToImage)
            context.startActivity(intent)
        }
    }
}