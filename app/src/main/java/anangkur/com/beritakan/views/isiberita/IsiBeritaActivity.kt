package anangkur.com.beritakan.views.isiberita

import anangkur.com.beritakan.R
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class IsiBeritaActivity : AppCompatActivity(), IsiBeritaView, View.OnClickListener{

    internal lateinit var presenter: IsiBeritaPresenter

    private var toolbar: Toolbar? = null
    private var txt_title_berita: TextView? = null
    private var img_berita: ImageView? = null
    private var txt_isi_berita: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_isi_berita)
        defineViews()
        changeFonts()
        initPresenter()
        onAttachView()
    }

    private fun initPresenter(){
        presenter = IsiBeritaPresenter()
    }

    override fun onDestroy() {
        onDetachView()
        super.onDestroy()
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.onShowFragment()
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    private fun defineViews(){
        toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        toolbar?.navigationIcon = resources.getDrawable(R.drawable.ic_back)
        toolbar?.setNavigationOnClickListener { onBackClick() }

        txt_title_berita = findViewById(R.id.txt_title_berita)
        img_berita = findViewById(R.id.img_berita)
        txt_isi_berita = findViewById(R.id.txt_isi_berita)
    }

    private fun changeFonts(){
        val chapaza = Typeface.createFromAsset(this?.getAssets(), "Chapaza.ttf")

        txt_title_berita?.setTypeface(chapaza)
    }

    override fun onShowFragment() {
        txt_title_berita?.text = intent.getStringExtra("title")
        Picasso.with(this)
            .load(intent.getStringExtra("urlToImage"))
            .fit()
            .centerCrop()
            .into(img_berita)
        txt_isi_berita?.text = intent.getStringExtra("content")
    }

    override fun onClick(v: View?) {

    }

    private fun onBackClick(){
        onBackPressed()
    }
}
