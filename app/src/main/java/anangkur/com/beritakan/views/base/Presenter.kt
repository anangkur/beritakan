package anangkur.com.beritakan.views.base

interface Presenter<T : View> {
    fun onAttach(view:T)
    fun onDetach()
}