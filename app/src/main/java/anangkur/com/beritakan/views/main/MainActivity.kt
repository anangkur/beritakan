package anangkur.com.beritakan.views.main

import anangkur.com.beritakan.R
import anangkur.com.beritakan.models.Article
import anangkur.com.beritakan.models.ResponseApiHome
import anangkur.com.beritakan.views.isiberita.IsiBeritaActivity
import anangkur.com.beritakan.views.main.adapters.SubheadlineNewsAdapter
import android.content.Intent
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.*
import com.squareup.picasso.Picasso

class MainActivity:AppCompatActivity(), MainView, View.OnClickListener {

    private lateinit var presenter: MainPresenter

    private var toolbar: Toolbar? = null
    private var txt_title_headline: TextView? = null
    private var img_headline: ImageView? = null
    private var txt_isi_headline: TextView? = null
    private var btn_bacaselengkapnya: TextView? = null
    private var img_subheadline1: ImageView? = null
    private var txt_title_subheadline1: TextView? = null
    private var img_subheadline2: ImageView? = null
    private var txt_title_subheadline2: TextView? = null
    private var img_subheadline3: ImageView? = null
    private var txt_title_subheadline3: TextView? = null
    private var img_subheadline4: ImageView? = null
    private var txt_title_subheadline4: TextView? = null
    private var recycler_bisnis: RecyclerView? = null
    private var recycler_olahraga: RecyclerView? = null
    private var recycler_teknologi: RecyclerView? = null
    private var recycler_hiburan: RecyclerView? = null

    private var layout_subheadline1: LinearLayout? = null
    private var layout_subheadline2: LinearLayout? = null
    private var layout_subheadline3: LinearLayout? = null
    private var layout_subheadline4: LinearLayout? = null

    private var breakingData: ResponseApiHome? = null

    private var subheadlineNewsAdapter: SubheadlineNewsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        defineViews()
        changeFonts()
        initPresenter()
        onAttachView()
    }

    private fun initPresenter() {
        presenter = MainPresenter()
    }

    override fun onDestroy() {
        onDetachView()
        super.onDestroy()
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.showFragment()
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    private fun defineViews(){
        toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Beritakan"

        txt_title_headline = findViewById(R.id.txt_title_headline)
        img_headline = findViewById(R.id.img_headline)
        txt_isi_headline = findViewById(R.id.txt_isi_headline)
        btn_bacaselengkapnya = findViewById(R.id.btn_bacaselengkapnya)

        img_subheadline1 = findViewById(R.id.img_subheadline1)
        txt_title_subheadline1 = findViewById(R.id.txt_title_subheadline1)

        img_subheadline2 = findViewById(R.id.img_subheadline2)
        txt_title_subheadline2 = findViewById(R.id.txt_title_subheadline2)

        img_subheadline3 = findViewById(R.id.img_subheadline3)
        txt_title_subheadline3 = findViewById(R.id.txt_title_subheadline3)

        img_subheadline4 = findViewById(R.id.img_subheadline4)
        txt_title_subheadline4 = findViewById(R.id.txt_title_subheadline4)

        recycler_bisnis = findViewById(R.id.recycler_bisnis)
        recycler_olahraga = findViewById(R.id.recycler_olahraga)
        recycler_teknologi = findViewById(R.id.recycler_teknologi)
        recycler_hiburan = findViewById(R.id.recycler_hiburan)

        layout_subheadline1 = findViewById(R.id.layout_subheadline1)
        layout_subheadline2 = findViewById(R.id.layout_subheadline2)
        layout_subheadline3 = findViewById(R.id.layout_subheadline3)
        layout_subheadline4 = findViewById(R.id.layout_subheadline4)

        btn_bacaselengkapnya?.setOnClickListener { onClickBtnSelengkapnya() }
        txt_isi_headline?.setOnClickListener { onClickBtnSelengkapnya() }
        img_headline?.setOnClickListener { onClickBtnSelengkapnya() }
        txt_title_headline?.setOnClickListener { onClickBtnSelengkapnya() }
        layout_subheadline1?.setOnClickListener { breakingData?.articles?.get(1)?.let { it1 -> onClickSubheadline(it1) } }
        layout_subheadline2?.setOnClickListener { breakingData?.articles?.get(2)?.let { it1 -> onClickSubheadline(it1) } }
        layout_subheadline3?.setOnClickListener { breakingData?.articles?.get(3)?.let { it1 -> onClickSubheadline(it1) } }
        layout_subheadline4?.setOnClickListener { breakingData?.articles?.get(4)?.let { it1 -> onClickSubheadline(it1) } }
    }

    private fun changeFonts(){
        val chapaza = Typeface.createFromAsset(this?.getAssets(), "Chapaza.ttf")

        txt_title_headline?.setTypeface(chapaza)
        txt_title_subheadline1?.setTypeface(chapaza)
        txt_title_subheadline2?.setTypeface(chapaza)
        txt_title_subheadline3?.setTypeface(chapaza)
        txt_title_subheadline4?.setTypeface(chapaza)
    }

    override fun onBreakingNewsReady(breakingData: ResponseApiHome?) {

        this.breakingData = breakingData

        txt_title_headline?.text = breakingData?.articles?.get(0)?.title
        Picasso.with(this)
            .load(breakingData?.articles?.get(0)?.urlToImage)
            .fit()
            .centerCrop()
            .into(img_headline)
        txt_isi_headline?.text = breakingData?.articles?.get(0)?.description

        txt_title_subheadline1?.text = breakingData?.articles?.get(1)?.title
        Picasso.with(this)
            .load(breakingData?.articles?.get(1)?.urlToImage)
            .fit()
            .centerCrop()
            .into(img_subheadline1)

        txt_title_subheadline2?.text = breakingData?.articles?.get(2)?.title
        Picasso.with(this)
            .load(breakingData?.articles?.get(2)?.urlToImage)
            .fit()
            .centerCrop()
            .into(img_subheadline2)

        txt_title_subheadline3?.text = breakingData?.articles?.get(3)?.title
        Picasso.with(this)
            .load(breakingData?.articles?.get(3)?.urlToImage)
            .fit()
            .centerCrop()
            .into(img_subheadline3)

        txt_title_subheadline4?.text = breakingData?.articles?.get(4)?.title
        Picasso.with(this)
            .load(breakingData?.articles?.get(4)?.urlToImage)
            .fit()
            .centerCrop()
            .into(img_subheadline4)
    }

    override fun onSportsNewsReady(sportsData: ResponseApiHome?) {
        subheadlineNewsAdapter = sportsData?.articles?.let { SubheadlineNewsAdapter(it) }
        val layoutManagerBestSeller = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler_olahraga?.setLayoutManager(layoutManagerBestSeller)
        recycler_olahraga?.setItemAnimator(DefaultItemAnimator())
        recycler_olahraga?.setAdapter(subheadlineNewsAdapter)
    }

    override fun onBusinessNewsReady(businessData: ResponseApiHome?) {
        subheadlineNewsAdapter = businessData?.articles?.let { SubheadlineNewsAdapter(it) }
        val layoutManagerBestSeller = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler_bisnis?.setLayoutManager(layoutManagerBestSeller)
        recycler_bisnis?.setItemAnimator(DefaultItemAnimator())
        recycler_bisnis?.setAdapter(subheadlineNewsAdapter)
    }

    override fun onTechNewsReady(techData: ResponseApiHome?) {
        subheadlineNewsAdapter = techData?.articles?.let { SubheadlineNewsAdapter(it) }
        val layoutManagerBestSeller = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler_teknologi?.setLayoutManager(layoutManagerBestSeller)
        recycler_teknologi?.setItemAnimator(DefaultItemAnimator())
        recycler_teknologi?.setAdapter(subheadlineNewsAdapter)
    }

    override fun onEntertainmentNewsReady(entertainmentData: ResponseApiHome?) {
        subheadlineNewsAdapter = entertainmentData?.articles?.let { SubheadlineNewsAdapter(it) }
        val layoutManagerBestSeller = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler_hiburan?.setLayoutManager(layoutManagerBestSeller)
        recycler_hiburan?.setItemAnimator(DefaultItemAnimator())
        recycler_hiburan?.setAdapter(subheadlineNewsAdapter)
    }

    override fun onClick(v: View?) {

    }

    override fun goToisiBeritaActivity(berita: Article) {
        intent = Intent(this, IsiBeritaActivity::class.java)
        intent.putExtra("author", berita.author)
        intent.putExtra("content", berita.content)
        intent.putExtra("description", berita.description)
        intent.putExtra("publishedAt", berita.publishedAt)
        intent.putExtra("source", berita.source?.name)
        intent.putExtra("title", berita.title)
        intent.putExtra("url", berita.url)
        intent.putExtra("urlToImage", berita.urlToImage)
        startActivity(intent)
    }

    override fun onClickBtnSelengkapnya() {
        breakingData?.articles?.get(0)?.let { presenter.onClickBtnSelengkapnya(it) }
    }

    override fun onClickSubheadline(berita: Article) {
        presenter.onClickSubheadline(berita)
    }
}
