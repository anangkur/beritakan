package anangkur.com.beritakan.views.splash

import anangkur.com.beritakan.views.base.View

interface SplashView: View {
    fun onShowFragment()
}