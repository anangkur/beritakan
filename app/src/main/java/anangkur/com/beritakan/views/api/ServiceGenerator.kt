package anangkur.com.beritakan.views.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

object ServiceGenerator {

    private val builder = RetrofitHelper.getBuilder("http://newsapi.org/v2/")
    private var retrofit = RetrofitHelper.getClient("http://newsapi.org/v2/")

    private val logging = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    private val httpClient = OkHttpClient.Builder()

    fun <S> createService(
        serviceClass: Class<S>
    ): S? {
        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging)
            builder?.client(httpClient.build())
            retrofit = builder?.build()
        }

        return retrofit?.create(serviceClass)
    }
}