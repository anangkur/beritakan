package anangkur.com.beritakan.views.splash

import anangkur.com.beritakan.views.base.Presenter

class SplashPresenter: Presenter<SplashView> {
    private var mView: SplashView? = null
    override fun onAttach(view: SplashView) {
        mView = view
    }
    override fun onDetach() {
        mView = null
    }
    fun showFragment() {
        // Set Data
        // Show Fragment with Data
        mView?.onShowFragment()
    }
}