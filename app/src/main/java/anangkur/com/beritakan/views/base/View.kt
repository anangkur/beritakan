package anangkur.com.beritakan.views.base

interface View{
    fun onAttachView()
    fun onDetachView()
}