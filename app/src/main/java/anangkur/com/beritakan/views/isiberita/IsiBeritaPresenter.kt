package anangkur.com.beritakan.views.isiberita

import anangkur.com.beritakan.views.base.Presenter

class IsiBeritaPresenter:Presenter<IsiBeritaView> {

    private var mView: IsiBeritaView? = null

    override fun onAttach(view: IsiBeritaView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun onShowFragment(){
        mView?.onShowFragment()
    }
}