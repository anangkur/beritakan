package anangkur.com.beritakan.views.isiberita

import anangkur.com.beritakan.views.base.View

interface IsiBeritaView:View {
    fun onShowFragment()
}