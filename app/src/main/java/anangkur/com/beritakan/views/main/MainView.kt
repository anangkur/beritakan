package anangkur.com.beritakan.views.main

import anangkur.com.beritakan.models.Article
import anangkur.com.beritakan.models.ResponseApiHome
import anangkur.com.beritakan.views.base.View

interface MainView:View{
    fun onBreakingNewsReady(breakingData: ResponseApiHome?)
    fun onSportsNewsReady(sportsData: ResponseApiHome?)
    fun onBusinessNewsReady(businessData: ResponseApiHome?)
    fun onTechNewsReady(techData: ResponseApiHome?)
    fun onEntertainmentNewsReady(entertainmentData: ResponseApiHome?)
    fun goToisiBeritaActivity(berita: Article)
    fun onClickBtnSelengkapnya()
    fun onClickSubheadline(berita: Article)
}